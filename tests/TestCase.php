<?php

namespace Tests;

use GuzzleHttp\Client;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected $token;

    public function getAuthHeader()
    {
        $response = $this->post('wx/auth/login', [
            'username' => '十月有风',
            'password' => '123456',
        ]);
        $token = $response->getOriginalContent()['data']['token'] ?? '';
        $this->token = $token;
        return ['Authorization' => "Bearer {$token}"];
    }

    public function assertLitemalApiGet($uri)
    {
        return $this->assertLitemalApi($uri);
    }

    public function assertLitemalApiPost($uri,$data = [])
    {
        return $this->assertLitemalApi($uri,'post',$data);
    }

    public function assertLitemalApi($uri, $method = 'get', $data = [])
    {

        $client = new Client();
        if ($method == 'get') {
            $response1 = $this->get($uri, $this->getAuthHeader());
//            $response2 = $client->get('http://coolcars.vips:8083/index.php/wx/address/list', [
//                'headers' => ['X-Lisemal-Token', $this->token]]);
        } else {
            $response1 = $this->post($uri, $data, $this->getAuthHeader());
//            $response2 = $client->post('http://coolcars.vips:8083/index.php/wx/address/list', [
//                'headers' => ['X-Lisemal-Token', $this->token],
//                'json' => $data]);
        }
//        $content = $response2->getBody()->getContents();
//        $content = json_decode($content, true);
//        $response1->assertJson($content);
        dd($response1->getOriginalContent());

    }
}
