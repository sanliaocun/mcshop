<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class CatelogTest extends TestCase
{

    use DatabaseTransactions;

    public function testIndex()
    {
        $this->assertLitemalApiGet('wx/catelog/index');
    }

    public function testCurrent()
    {
        $this->assertLitemalApiGet('wx/catelog/current');

    }
}
