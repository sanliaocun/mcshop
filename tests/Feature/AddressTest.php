<?php

namespace Tests\Feature;

use App\Models\Address;
use GuzzleHttp\Client;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AddressTest extends TestCase
{
    use DatabaseTransactions;

    public function testLists()
    {
        $this->assertLitemalApiGet('wx/address/list');
//        $response = $this->get('wx/address/list', $this->getAuthHeader());
//        dd($response->getOriginalContent());
//        $client = new Client();
//        $response2 = $client->get('http://coolcars.vips:8083/index.php/wx/address/list',[
//            'headers'=>['X-Lisemal-Token',$this->token]
//        ]);
//        $list = json_decode($response2->getBody()->getContents(),true);
//        $response->assertJson($list);
    }

    public function testDelete()
    {
        $address = Address::query()->first();
        $response = $this->post('wx/address/delete',['id'=>$address->id],
            $this->getAuthHeader());
        $response->assertJson(['errno'=>0]);
        $address = Address::query()->find($address->id);
        $this->assertEmpty($address);
    }

}
