<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class BeandTest extends TestCase
{
    use DatabaseTransactions;

    public function testDetail()
    {
        $this->assertLitemalApiGet('wx/brand/detail');
    }

    public function testList(){
        $this->assertLitemalApiGet('wx/brand/list');
    }
}
