<?php

namespace Tests\Feature;



use App\Services\UserServices;
use http\Client\Curl\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
class AuthTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * 注册测试
     *
     * @return void
     */

    public function testRegister()
    {
//        get方式请求
        $code = UserServices::getInstance()->setCaptcha(13160899123);
        $response = $this->post('wx/auth/register',[
            'username'=>'十月有风200',
            'password'=>'123456',
            'mobile'=>'13160899123',
            'code' => $code
        ]);
//        验证结果是否正确
//        $response->assertStatus(200);
//        $ret = $response->getOriginalContent();
//        //对比状态是否一致
//        $this->assertEquals(0,$ret['errno']);
////        判断结果是否非空
//        $this->assertNotEmpty($ret['data']);
//        获取响应的内容
        echo $response->getContent();

    }    /**
     * 参数验证码
     *
     * @return void
     */

    public function testRegCaptcha()
    {
//        get方式请求
        $response = $this->post('wx/auth/regCaptcha',[
            'mobile'=>'13160899107',
        ]);
//        验证结果里面的整个json
        $response->assertJson(['errno'=>0,'errmsg'=>'成功','data'=>null]);
//        验证结果是否正确
//        $response->assertStatus(200);
//        $ret = $response->getOriginalContent();
        //对比状态是否一致
//        $this->assertEquals(0,$ret['errno']);
//        判断结果是否非空
//        $this->assertNotEmpty($ret['data']);
//        获取响应的内容
//        echo $response->getContent();

    }

    public function testLogin(){
        $response = $this->post('wx/auth/login',[
            'username'=>'十月有风',
            'password'=>'123456',
        ]);
        dd($response->getOriginalContent());
    }

    public function testUser(){
        $response = $this->post('wx/auth/login',[
            'username'=>'十月有风',
            'password'=>'123456',
        ]);
        $token = $response->getOriginalContent()['data']['token'] ?? '';
        $response2 = $this->get('wx/auth/user',[
            'Authorization'=> "Bearer {$token}"
        ]);
        $response2->assertJson(['data'=>['username'=>"十月有风"]]);
    }

    public function testInfo(){
        $response = $this->post('wx/auth/login',[
            'username'=>'十月有风',
            'password'=>'123456',
        ]);
        $token = $response->getOriginalContent()['data']['token'] ?? '';
        $response2 = $this->get('wx/auth/info',[
            'Authorization'=> "Bearer {$token}"
        ]);
        $user = UserServices::getInstance()->getByUsername('十月有风');
        $response2->assertJson(['data'=>[
            'nickName' =>$user->nickname,
            'avatar' => $user->avatar,
            'gender' => $user->gender,
            'mobile' => $user->mobile,
        ]]);
    }
}
