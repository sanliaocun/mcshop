<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class GoodsTest extends TestCase
{
    use DatabaseTransactions;

    public function testCount()
    {
        $this->assertLitemalApiGet('wx/goods/count');
    }

    public function testCategory()
    {
        $this->assertLitemalApiGet('wx/goods/category?id=1008009');
        $this->assertLitemalApiGet('wx/goods/category?id=1005000');
    }


    public function testList()
    {
//        $this->assertLitemalApiGet('wx/goods/list');
//        $this->assertLitemalApiGet('wx/goods/list?categoryId=1008009');
        $this->assertLitemalApiGet('wx/goods/list?keyword=四件套');
//        $this->assertLitemalApiGet('wx/goods/list?isNew=1');
//        $this->assertLitemalApiGet('wx/goods/list?isHost=1');
//        $this->assertLitemalApiGet('wx/goods/list?page=2&limit=5');
    }

    public function testDtail(){
        $this->assertLitemalApiGet('wx/goods/detail?id=1181000');
    }
}
