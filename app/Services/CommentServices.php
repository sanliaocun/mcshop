<?php


namespace App\Services;


use App\Models\Comment;
use Illuminate\Support\Arr;

class CommentServices extends BaseServices
{

    public function getCommentByGoodsId($goodsId, $page = 1, $limit = 2,$sort = 'add_time',$order = 'desc')
    {
        return Comment::query()->where('value_id', $goodsId)
            ->where('type', 0)->where('deleted', 0)
            ->orderBy($sort,$order)
//            ->forPage($page,$limit)->get();
            ->paginate($limit, ['*'], 'page', $page);
    }

    public function getCommentWithUserInfo($goodsId, $page = 1, $limit = 2)
    {
        $comments = $this->getCommentByGoodsId($goodsId, $page, $limit);
        $userIds = Arr::pluck($comments->items(), 'user_id');
        $userIds = array_unique($userIds);
        $user = UserServices::getInstance()->getUsers($userIds)->keyBy('id');
//        dd($user);
        $data = collect($comments->items())->map(function (Comment $comment) use ($user) {
        $user = $user->get($comment->user_id);
        $comment = $comment->toArray();
        $comment['picList'] = $comment['picUrls'];
        $comment = Arr::only($comment,['id','addTime','content','adminContent','picList','']);
        $comment['nickname'] = $user->nickname ?? '';
        $comment['avatar'] = $user->avatar ?? '';
        return $comment;
//            return [
//                'id' => $comment->id,
//                'addTime' => $comment->add_time,
//                'content' => $comment->content,
//                'adminContent' => $comment->admin_content,
//                'picList' => $comment->pic_list,
//                'nickname' => $user->nickname,
//                'avatar' => $user->avatar,
//            ];
        });
        return ['count'=>$comments->total(),'data'=>$data];
    }
}
