<?php


namespace App\Services;


use App\Models\Category;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class CatelogServices extends BaseServices
{
    /**
     * @return Builder[]|Collection
     */
    public function getL1List()
    {
        return Category::query()->where('level', 'L1')
            ->where('deleted', 0)
            ->get();
    }

    /**
     * @param int $pid
     * @return Builder[]|Collection
     */
    public function getL2ListByPid(int $pid)
    {
        return Category::query()->where('level', 'L2')
            ->where('pid', $pid)->where('deleted', 0)
            ->get();
    }

    /**
     * @param int $id
     * @return Category|null|mode
     */
    public function getL1ById(int $id)
    {
        return Category::query()->where('level', 'L1')
            ->where('id', $id)->where('deleted', 0)
            ->first();
    }

    public function getCategoryById(int $id)
    {
        return Category::query()->find($id);
    }

    public function getCagegory(int $id)
    {
        return Category::query()->find($id);
    }


    public function getL2ListByIds(array $ids)
    {
        if(empty($ids)){
            return collect([]);
        }
        return Category::query()->whereIn('id',$ids)->get();
    }
}
