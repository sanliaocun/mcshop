<?php


namespace App\Services;


use App\CodeResponse;
use App\Exceptions\BusinessException;
use App\Notifications\VerificationCode;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\Support\Facades\Notification;
use Leonis\Notifications\EasySms\Channels\EasySmsChannel;
use Overtrue\EasySms\PhoneNumber;

class UserServices extends BaseServices
{

    public function getUsers(array $userIds)
    {
        if(empty($userIds)){
            return collect([]);
        }
        return User::query()->whereIn('id',$userIds)
            ->where('deleted',0)->get();
    }

    /**
     * @param $username
     * 根据用户获取信息
     * @return null
     **/
    public function getByUsername($username)
    {
        return User::query()->where('username', $username)
            ->where('deleted', 0)->first();
    }

    /**
     * @param $mobile
     * 根据手机号获取信息
     * @return null
     **/
    public function getByMobile($mobile)
    {
        return User::query()->where('mobile', $mobile)
            ->where('deleted', 0)->first();
    }


    /**
     * @param $mobile
     * 验证手机号发送验证码是否达到限制条数
     * @return null
     **/
    public function checkMobileSendCaptchCount(string $mobile)
    {
        $countKey = 'register_captcha_count_' . $mobile;
        if (Cache::has($countKey)) {
            $count = Cache::increment($countKey);
            if ($count > 10) {
                return false;
            }
        } else {
            Cache::put($countKey, 1, Carbon::tomorrow()->diffInSeconds(now()));
        }
        return true;
    }

    /**
     * @param $mobile
     * 发送验证码短信
     * @return null
     **/
    public function sendCaptchaMsg(string $mobile, $code)
    {
        if (app()->environment('testing')) {
            return;
        }
        //todo发送短信
        Notification::route(EasySmsChannel::class,
            new PhoneNumber($mobile, 86))
            ->notify(new VerificationCode($code));
    }

    public function checkCaptcha($mobile, $code)
    {

//        判断在非生产环境下面
        if (!app()->environment('production')) {
            return true;
        }
        $key = 'register_captcha_' . $mobile;
        $isPass = $code === Cache::get($key);
        if ($isPass) {
            Cache::forget($key);
            return true;
        } else {
            throw new BusinessException(CodeResponse::AUTH_CAPTCHA_UNMATCH);
        }

//        $key = 'register_captcha_'.$mobile;
//        $value = Cache::get($key);
//        $isPass = $code === $value;
//        if($isPass){
//            Cache::forget($key);
//            return true;
//        }else{
//            throw new BusinessException(CodeResponse::AUTH_CAPTCHA_UNMATCH);
//        }
    }

    public function setCaptcha(string $mobile)
    {
        //todo随机生成6位验证码
        $code = random_int(1000000, 9999999);
        $code = strval($code);
        //保存验证的有效性10分钟
        Cache::put('register_captcha_' . $mobile, $code, 600);
        return $code;
    }

}
