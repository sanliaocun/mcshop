<?php


namespace App\Services;


use App\Models\Brand;
use Illuminate\Database\Query\Builder;

class BrandServices extends BaseServices
{

    public function getBrand(int $id)
    {
        return Brand::query()->find($id);
    }

    public function getBeandList(int $page, int $limit, $sort, $order,$columns =['*'])
    {
//        return Brand::query()->where('deleted',0)
//            ->when(!empty($sort) && !empty($order), function (Builder $query) use ($sort,$order){
//                return $query->orderBy($sort,$order);
//            })->paginate($limit,$columns,'page',$page);

        $query = Brand::query()->where('deleted', 0);
        if (!empty($sort) && !empty($order)) {
            $query = $query->orderBy($sort, $order);
        }
        return $query->paginate($limit, $columns, 'page', $page);
    }

}
