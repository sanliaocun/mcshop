<?php


namespace App\Services;


use App\Models\Footprint;
use App\Models\Goods;
use App\Models\GoodsAttribute;
use App\Models\GoodsProduct;
use App\Models\GoodsSpecification;
use App\Models\Issue;
use Illuminate\Database\Eloquent\Builder;


class GoodsServices extends BaseServices
{
    /**查询商品详情**/
    public function getGoods(int $id)
    {
        return Goods::query()->find($id);
    }

    /**获取商品属性**/
    public function getGoodsAttribute(int $goodsId)
    {
        return GoodsAttribute::query()->where('goods_id', $goodsId)
            ->where('deleted', 0)->get();
    }

    /**获取商品规格**/
    public function getGoodsSpecification(int $goodsId)
    {
        $spec = GoodsSpecification::query()->where('goods_id', $goodsId)
            ->where('deleted', 0)->get()->groupBy('specification');
        return $spec->map(function ($v, $k) {
            return ['name' => $k, 'valueList' => $v];
        })->values();
    }

    public function getGoodsProduct(int $goodsId)
    {
        return GoodsProduct::query()->where('goods_id', $goodsId)
            ->where('deleted', 0)->get();
    }

    /**获取问题信息*/
    public function getGoodsIssue($page = 1, $limit = 4)
    {
        return Issue::query()->forPage($page, $limit)->get();
    }

    public function saveFootprint($userId,$goodsId)
    {
        $footPrint = new Footprint();
        $footPrint->fill([
           'user_id' => $userId,
           'goods_id' => $goodsId
        ]);
        return $footPrint->save();
    }

    public function countGoodsOnSale()
    {
        return Goods::query()->where('is_on_sale', 1)
            ->where('deleted', 0)->count('id');
    }

    /**获取订单列表**/
    public function listGoods($categoryId,
                              $brandId,
                              $isNew,
                              $isHost,
                              $keyword,
                              $columns = ['*'],
                              $sort = 'add_time',
                              $order = 'asc',
                              $page = 1,
                              $limit = 10)
    {

        $query = $this->getQueryByGoodsFilter($brandId, $isNew, $isHost, $keyword);
        if (!empty($categoryId)) {
            $query = $query->where('category_id', $categoryId);
        }
        return $query->orderBy($sort, $order)
            ->paginate($limit, $columns, 'page', $page);
    }

    public function list2Category($brandId, $isNew, $isHost, $keyword)
    {
        $query = $this->getQueryByGoodsFilter($brandId, $isNew, $isHost, $keyword);
        $categoryIds = $query->select(['category_id'])->pluck('category_id')->toArray();
        return CatelogServices::getInstance()->getL2ListByIds($categoryIds);
    }


    private function getQueryByGoodsFilter($brandId, $isNew, $isHost, $keyword)
    {
        $query = Goods::query()->where('is_on_sale', 1)->where('deleted', 0);
        if (!empty($brandId)) {
            $query = $query->where('category_id', $brandId);
        }

        if (!empty($isNew)) {
            $query = $query->where('is_new', $isNew);
        }
        if (!empty($isHost)) {
            $query = $query->where('is_host', $isHost);
        }

        if (!empty($keyword)) {
            $query = $query->where(function (Builder $query) use ($keyword) {
                $query->where('keywords', 'like', "%$keyword%")
                    ->orWhere('name', 'like', "%$keyword%");
            });
        }
        return $query;
    }
}
