<?php


namespace App\Services;


use App\Models\SearchHistory;

class SearchHistoryServices extends BaseServices
{
    public function save($userId,$keyweod,$from)
    {
        $history = new SearchHistory();
        $history->fill([
            'user_id' => $userId,
            'keyword' => $keyweod,
            'from'    => $from,
        ]);
        $history->save();
        return $history;
    }
}
