<?php


namespace App\Services;


use App\CodeResponse;
use App\Exceptions\BusinessException;
use App\Models\Address;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class AddressServices extends BaseServices
{
    /**
     * @param int $userId
     * @return Address[]|callable
     **/
    public function getAddressListByUserId(int $userId)
    {

        return Address::query()->where('user_id', $userId)
            ->where('deleted', 0)->get();
    }

    /**
     * 获取用户地址
     * @param $userId
     * @param $addressId
     * @return Address|Model|null
     **/
    public function getAddress($userId, $addressId)
    {
        return Address::query()->where('user_id', $userId)
            ->where('id', $addressId)->where('deleted', 0)
            ->first();
    }

    public function delete($userId, $addressId)
    {
        $address = $this->getAddress($userId,$addressId);
        if(is_null($address)){
            $this->throwBusinessException(CodeResponse::PARAM_ILLEGAL);
        }
        return $address->delete();
    }
}
