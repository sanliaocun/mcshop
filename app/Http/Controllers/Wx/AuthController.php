<?php


namespace App\Http\Controllers\Wx;


use App\CodeResponse;
use App\Models\User;
use App\Services\UserServices;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class AuthController extends WxController
{

    protected $only = ['user','profile','info'];

    public function info()
    {
        $user = Auth::guard('wx')->user();
        return $this->success([
            'nickName' => $user->nickname,
            'avatar' => $user->avatar,
            'gender' => $user->gender,
            'mobile' => $user->mobile,
        ]);
    }

    public function profile(Request $request)
    {
        $user = Auth::guard('wx')->user();

        $avatar = $request->input('avatar');
        $gender = $request->input('gender');
        $nickname = $request->input('nickname');

        if(!empty($avatar)){
            $user->avatar = $avatar;
        }
        if(!empty($gender)){
            $user->gender = $gender;
        }
        if(!empty($nickname)){
            $user->nickname = $nickname;
        }
        $ret = $user->save();
        return $this->failOrSuccess($ret,CodeResponse::UPDATE_FILE);
    }

    public function users()
    {
        $user = Auth::guard('wx')->user();
        return $this->success($user);
    }


    /**
     * 登出
     **/
    public function logout()
    {
        Auth::guard('wx')->logout();
        return $this->success();
    }

    public function reset(Request $request)
    {
        $password = $request->input('password');
        $mobile = $request->input('mobile');
        $code = $request->input('code');

        if (empty($password) || empty($mobile) || empty($code)) {
            return $this->fail(CodeResponse::PARAM_ILLEGAL);
        }

        $isPass = UserServices::getInstance()->checkCaptcha($mobile, $code);
        if (!$isPass) {
            return $this->fail(CodeResponse::AUTH_CAPTCHA_UNMATCH);
        }

        $user = UserServices::getInstance()->getByUsername($mobile);
        if (is_null($user)) {
            return $this->fail(CodeResponse::AUTH_MOBILE_UNREGISTERED);
        }

        $user->password = Hash::make($password);
        $ret = $user->save();
        return $this->failOrSuccess($ret, CodeResponse::UPDATE_FILE);
    }

    /**
     * 登录
     **/
    public function login(Request $request)
    {
//        获取账号密码
        $username = $request->input('username');
        $password = $request->input('password');
        //        数据验证
        if (empty($username) || empty($password)) {
            return $this->fail(CodeResponse::PARAM_ILLEGAL);
        }
//        验证账号是否存在
        $user = UserServices::getInstance()->getByUsername($username);
        if (is_null($user)) {
            return $this->fail(CodeResponse::AUTH_INVALID_ACCOUNT);
        }
//        对密码进行验证
        $isPass = Hash::check($password, $user->password);
        if (!$isPass) {
            return $this->fail(CodeResponse::AUTH_INVALID_ACCOUNT, '账户密码不对');
        }
//        更新登录信息
        $user->last_login_time = now()->toDateTimeString();
        $user->last_login_ip = $request->getClientIp();
        if (!$user->save()) {
            return $this->fail(CodeResponse::UPDATE_FILE);
        }
//        获取token
        $token = Auth::guard('wx')->login($user);
//        组装数据并返回
        return $this->success([
            'token' => $token,
            'userinfo' => [
                'nickname' => $username,
                'avatarUrl' => '',
            ]]);
    }

    /**
     * 注册
     **/
    public function register(Request $request)
    {

        //todo 获取参数
        $username = $request->input('username');
        $password = $request->input('password');
        $mobile = $request->input('mobile');
        $code = $request->input('code');
        //todo 验证参数是否为空
        if (empty($username) || empty($password) || empty($mobile) || empty($code)) {
            return $this->fail(CodeResponse::PARAM_ILLEGAL);
        }

        //todo 验证码是否为空
        $user = UserServices::getInstance()->getByUsername($username);

        if (!is_null($user)) {
            return $this->fail(CodeResponse::AUTH_NAME_REGISTERED);
        }

        $vaildator = Validator::make(['mobile' => $mobile], ['mobile' => 'regex:/^1[0-9]{10}$/']);
        if ($vaildator->fails()) {
            return $this->fail(CodeResponse::AUTH_INVALID_MOBILE);
        }

        //todo验证手机号是否已经被注册
        $mobiles = UserServices::getInstance()->getByMobile($mobile);
        if (!is_null($mobiles)) {
            return $this->fail(CodeResponse::AUTH_MOBILE_REGISTERED);
        }

        UserServices::getInstance()->checkCaptcha($mobile, $code);


        //todo 写入用户表
        $user = new User();
        $user->username = $username;
        $user->password = Hash::make($password);
        $user->mobile = $mobile;
        $user->avatar = '';
        $user->nickname = $username;
        $user->last_login_time = Carbon::now()->toDateTimeString();
        $user->last_login_ip = $request->getClientIp();
        $user->save();

        return $this->success([
            'token' => '',
            'userinfo' => [
                'nickname' => $username,
                'avatarUrl' => '',
            ]]);
    }


//    获取发送短信验证码
    public function regCaptcha(Request $request)
    {

        //todo获取手机号
        $mobile = $request->input('mobile');
        //todo验证手机号是否合法
        if (empty($mobile)) {
            return $this->fail(CodeResponse::PARAM_ILLEGAL);
        }
        $vaildator = Validator::make(['mobile' => $mobile], ['mobile' => 'regex:/^1[0-9]{10}$/']);
        if ($vaildator->fails()) {
            return $this->fail(CodeResponse::AUTH_INVALID_MOBILE);
        }
        //todo验证手机号是否已经被注册
        $mobiles = UserServices::getInstance()->getByMobile($mobile);
        if (!is_null($mobiles)) {
            return $this->fail(CodeResponse::AUTH_MOBILE_REGISTERED);
        }
        //todo保存手机和验证码的关系
        $lonk = Cache::add('register_captcha_lock_' . $mobile, 1, 60);
        if (!$lonk) {
            return $this->fail(CodeResponse::AUTH_CAPTCHA_FREQUENCY);
        }

        $isPass = UserServices::getInstance()->checkMobileSendCaptchCount($mobile);
        if (!$isPass) {
            return $this->fail(CodeResponse::AUTH_CAPTCHA_FREQUENCY, '验证码当天不能超过10次');
        }

        $code = UserServices::getInstance()->setCaptcha($mobile);
        //todo发送短信
        UserServices::getInstance()->sendCaptchaMsg($mobile, $code);

        return $this->success();
    }
}
