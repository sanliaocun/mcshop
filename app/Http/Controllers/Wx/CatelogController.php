<?php


namespace App\Http\Controllers\Wx;


use App\CodeResponse;
use App\Services\CatelogServices;
use Illuminate\Http\Request;

class CatelogController extends WxController
{

    protected $only = [];

    public function index(Request $request)
    {
        $id = $request->input('id', 0);

        $l1list = CatelogServices::getInstance()->getL1List();
        if (empty($id)) {
            $current = $l1list->first();
        } else {
            $current = $l1list->where('id', $id)->first();
        }

        $l2List = null;
        if (!is_null($current)) {
            $l2List = CatelogServices::getInstance()->getL2ListByPid($current->id);
        }

        return $this->success([
            'categoryList' => $l1list->toArray(),
            'currentCategory' => $current,
            'currentSubCategory' => $l2List->toArray()
        ]);
    }

    public function current(Request $request)
    {
        $id = $request->input('id', 0);
        if (empty($id)) {
            return $this->fail(CodeResponse::PARAM_VALUE_ILLEGAL);
        }

        $category = CatelogServices::getInstance()->getL1ById($id);
        if (is_null($category)) {
            return $this->fail(CodeResponse::PARAM_VALUE_ILLEGAL);
        }

        $l2list = CatelogServices::getInstance()->getL2ListByPid($category->id);
        return $this->success([
            'currentCategory' => $category,
            'currentSubCategory' => $l2list->toArray(),
        ]);
    }
}
