<?php


namespace App;


class Constant
{
    const SEARCH_HISTORY_FROM_WX = 'wx';
    const SEARCH_HISTORY_FROM_APP = 'app';
    const SEARCH_HISTORY_FROM_PC = 'pc';

    const COLLECT_TYPE_GOODS = 0;
    const COLLECT_TYPE_TOPIC = 1;

    const COMMENT_TYPE_GOODS = 0;
    const COMMENT_TYPE_TOPIC = 1;
}
