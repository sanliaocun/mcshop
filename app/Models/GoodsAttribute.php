<?php


namespace App\Models;


class GoodsAttribute extends BaseModel
{
    protected $table = 'goods_attribute';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    protected $casts = [
        'deleted' => 'boolean',
    ];

}
