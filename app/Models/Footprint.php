<?php


namespace App\Models;


class Footprint extends BaseModel
{
    protected $table = 'footprint';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'goods_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    protected $casts = [
        'deleted' => 'boolean',
    ];
}
