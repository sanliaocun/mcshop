<?php


namespace App\Models;


class Goods extends BaseModel
{
    protected $table = 'goods';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    protected $casts = [
        'deleted'   => 'boolean',
        'is_new'    => 'boolean',
        'is_hot'    =>'boolean',
        'counter_price' =>'float',
        'retail_price'   => 'float',
        'gallery' => 'array',
        'is_on_sale' => 'boolean',
    ];

}
