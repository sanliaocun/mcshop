<?php


namespace App\Models;

class Brand extends BaseModel
{

    protected $table = 'brand';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    protected $casts = [
        'deleted' => 'boolean',
        'floor_price' => 'float',
    ];

}
