<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//注册
use App\Http\Controllers\Wx\AuthController;

Route::post('auth/register', [AuthController::class, 'register']);
Route::post('auth/regCaptcha', [AuthController::class, 'regCaptcha']);
Route::post('auth/login', [AuthController::class, 'login']);
Route::post('auth/user', [AuthController::class, 'users']);
Route::get('auth/info', [AuthController::class, 'info']);       //用户信息
Route::post('auth/logout', [AuthController::class, 'logout']);   //账号登出
Route::post('auth/profile', [AuthController::class, 'profile']); //账户修改
Route::post('auth/reset', [AuthController::class, 'reset']);     //账户密码重置
Route::post('auth/captcha', [AuthController::class, 'regCaptcha']); //验证码

//地址
use App\Http\Controllers\Wx\AddressController;
Route::get('address/list', [AddressController::class, 'lists']); //列表
Route::get('address/detail', [AddressController::class, 'detail']); //详情
Route::post('address/save', [AddressController::class, 'save']);    //更新
Route::post('address/delete', [AddressController::class, 'delete']); //删除

//类目
use App\Http\Controllers\Wx\CatelogController;
Route::get('catelog/index', [CatelogController::class, 'index']);
Route::get('catelog/current', [CatelogController::class, 'current']);

//品牌
use App\Http\Controllers\Wx\BrandController;
Route::get('brand/list', [BrandController::class, 'lists']);
Route::get('brand/detail', [BrandController::class, 'detail']);

use App\Http\Controllers\Wx\GoodsController;
Route::get('goods/count', [GoodsController::class, 'count']);
Route::get('goods/list', [GoodsController::class, 'lists']);
Route::get('goods/category', [GoodsController::class, 'category']);
Route::get('goods/detail', [GoodsController::class, 'detail']);






